let pg =
      https://raw.githubusercontent.com/pitgull/pitgull/v0.0.2/dhall/pitgull.dhall sha256:65a46e78c2d4aac7cd3afeb1fa209ed244dc60644634a9cfc61800ea3417ea9b

let scalaSteward
    : pg.Rule
    = { name = "Scala Steward"
      , matcher =
          pg.match.Many
            [ pg.match.Author
                { email = pg.text.Equals { value = "scala.steward@ocado.com" } }
            , pg.match.Description
                { text = pg.text.Matches { regex = ".*labels:.*semver-patch.*" }
                }
            , pg.match.PipelineStatus "success"
            ]
      , action = pg.action.Merge
      }

in  { scalaSteward }
